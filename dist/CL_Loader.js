/**
 * 动态加载引擎
 * Author: 陆楚良
 * Version: 1.2.3
 * Date: 2015/09/10
 * QQ: 874449204
 *
 * https://git.oschina.net/luchg/CL_Loader.js.git
 *
 * License: http://www.apache.org/licenses/LICENSE-2.0
 */


!function(window,document,undefined){
"use strict";

var CL_Loader = function(){return LP.run.apply(new Loader(), [].slice.call(arguments))};
CL_Loader.version = "1.2.3";

/*快捷类型判断*/
var Is = (function(){
    var class2type = {},i,
        L = "Boolean Number String Function Array Date RegExp Object Error".split(" ");
    for(i=0;i<9;i++){
        class2type[ "[object " + L[i] + "]" ] = L[i].toLowerCase();
    }
    var Type = function( obj ) {
        if ( obj == null ) {
            return String( obj );
        }
        return typeof obj === "object" || typeof obj === "function" ?
            class2type[ class2type.toString.call(obj) ] || "object" :
            typeof obj;
    };
    var f = function(t){return function(e){return Type(e)==t}};
    return {
        type: Type,
        Str : f("string"),
        Fun : f("function"),
        Obj : f("object"),
        Arr : f("array")
    }
})();

/*head节点*/
var head = document.head || document.getElementsByTagName("head")[0] || document.documentElement;
/*base节点*/
var baseElement = head.getElementsByTagName("base")[0];
/*老版本webkit*/
var isOldWebKit = +navigator.userAgent.replace(/.*(?:AppleWebKit|AndroidWebKit)\/(\d+).*/, "$1") < 536;

var console = function(){
    var __console__ = [];
    var console = Is.Obj(window.console) ? window.console : {};
    if(!Is.Fun(console.error)){console.error = function(w){__console__.push("error:"+w)}}
    return console;
}();
/*快捷便利数组*/
var each = function(a, f){
    for( var i=0; i<a.length; i++ )
        f.call(a, a[i], i);
};

// Queue 精简版
function Queue(){
    var list  = [];
    var canExecute = true;
    var timer = null;
    var self = this;
    self.queue = function(callback){
        list.push(callback);
        canExecute && self.dequeue();
        return self;
    };
    self.dequeue = function(){
        var self=self,s = list.shift();
        canExecute = false;
        if(s){
            clearTimeout(timer);
            timer = setTimeout(function(){
                Is.type(s)=="function" && s.call(self);
            },10);
        }else{
            canExecute = true;
        }
        return self;
    };
}

var _load = {
    getAbsPath : function(a){
        return function(url){
            a.href = url;
            return a.hasAttribute ? a.href : a.getAttribute("href", 4);
        };
    }(document.createElement("a")),
    cache : {},
    compile : function(file, charset, cache, onload){
        file.type = Is.Str(file.type) ? file.type.toLowerCase() : null;
        if( (file.type!="js" && file.type!="css") || !file.url || !Is.Str(file.url) ){
            // 重新严格验证是为了避免Loader中的私有属性被恶意修改
            onload(file);
            return;
        }
        file.charset = file.charset || charset;
        if(file.type=="css"){
            file.cache = true;
        }else if(Is.type(file.cache)!=="boolean"){
            file.cache =  cache;
        }
        var absPath = _load.getAbsPath(file.url);
        var Q2,Q1   = _load.cache[absPath];
        if(file.cache!==false && Q1){
            Q1.queue(function(){
                onload();
                Q1.dequeue();
            });
            return;
        }else if(!Q1){
            Q2 = _load.cache[absPath] = new Queue().queue(true);
        }
        _load.loadResource(file, function(){
            Q2 && Q2.dequeue();
            onload();
        });
    },

    //加载资源
    loadResource : function(file, onload){
        var node,
            isCSS = (file.type=='css'),
            c = Is.Str(file.charset) ? file.charset : null;
        var node = document.createElement(isCSS?"link":"script");
        if(isCSS){
            node.rel = "stylesheet";
            node.href= file.url;
        }else{
            node.async = true;
            node.src = file.url;
        }
        if (c){
            node.charset = c;
        }
        var supportOnload = "onload" in node,
            done = function(){
                node.onload = node.onreadystatechange = null;
                isCSS || head.removeChild(node);
                node=null;
                onload(file);
            };
        if(isCSS && (isOldWebKit || !supportOnload)){
            setTimeout(function(){_load.pollCss(node,done)},0);
        }else if(supportOnload){
            node.onload = done;
            node.onerror=function(){console.error("文件加载失败："+file.url);done()};
        }else{
            node.onreadystatechange = function() {
                /loaded|complete/.test(node.readyState) && done();
            }
        }
        baseElement ? head.insertBefore(node, baseElement) : head.appendChild(node);
    },

    pollCss : function(node,callback){
        var sheet = node.sheet,
            isLoaded;
          // for WebKit < 536
        if (isOldWebKit) {
            if (sheet) {
                isLoaded = true;
            }
        }
        // for Firefox < 9.0
        else if (sheet) {
            try {
                if (sheet.cssRules) {
                    isLoaded = true;
                }
            } catch (ex) {
                // The value of `ex.name` is changed from "NS_ERROR_DOM_SECURITY_ERR"
                // to "SecurityError" since Firefox 13.0. But Firefox is less than 9.0
                // in here, So it is ok to just rely on "NS_ERROR_DOM_SECURITY_ERR"
                if (ex.name === "NS_ERROR_DOM_SECURITY_ERR") {
                    isLoaded = true;
                }
            }
        }
        isLoaded ? callback() : setTimeout(function() {_load.pollCss(node, callback)}, 100);
    },

    //开始加载
    start : function(files, charset, queue, cache, callback){
        var num   = files.length;
        if(!num){
            callback();
            return;
        }
        if(queue===false){                                  // 并行加载
            each(files, function(file){
                _load.compile(file, charset, cache, function(){
                    num-=1;
                    num<=0 && callback();
                });
            });
        }else{                                              // 顺序加载，相应顺序由添加文件顺序以及数组顺序决定
            var Q=new Queue();
            each(files, function(file){
                Q.queue(function(){
                    _load.compile(file, charset, cache, function(){
                        num-=1;
                        num<=0 && callback();
                        Q.dequeue();
                    });
                });
            });
        }
    }
};

/*监控DOM加载*/
var Ready = function(){
    var Q = new Queue().queue(true),
        isReady = false,
        detach = function(){
            if ( document.addEventListener ) {
                document.removeEventListener( "DOMContentLoaded", completed, false );
                window.removeEventListener( "load", completed, false );

            } else {
                document.detachEvent( "onreadystatechange", completed );
                window.detachEvent( "onload", completed );
            }
        },
        completed = function(){
            // readyState === "complete" is good enough for us to call the dom ready in oldIE
            if ( document.addEventListener || event.type === "load" || document.readyState === "complete" ) {
                detach();
                done();
            }
        },
        done = function(){if( isReady ) return; isReady = true; Q.dequeue()};
    // 本段代码取自jQuery
    (function(){
        // Catch cases where $(document).ready() is called after the browser event has already occurred.
        // we once tried to use readyState "interactive" here, but it caused issues like the one
        // discovered by ChrisS here: http://bugs.jquery.com/ticket/12282#comment:15
        if ( document.readyState === "complete" ) {
            // Handle it asynchronously to allow scripts the opportunity to delay ready
            setTimeout( done, 0 );

        // Standards-based browsers support DOMContentLoaded
        } else if ( document.addEventListener ) {
            // Use the handy event callback
            document.addEventListener( "DOMContentLoaded", completed, false );

            // A fallback to window.onload, that will always work
            window.addEventListener( "load", completed, false );

        // If IE event model is used
        } else {
            // Ensure firing before onload, maybe late but safe also for iframes
            document.attachEvent( "onreadystatechange", completed );

            // A fallback to window.onload, that will always work
            window.attachEvent( "onload", completed );

            // If IE and not a frame
            // continually check to see if the document is ready
            var top = false;

            try {
                top = window.frameElement == null && document.documentElement;
            } catch(e) {}

            if ( top && top.doScroll ) {
                (function doScrollCheck() {
                    if ( !isReady ) {
                        try {
                            // Use the trick by Diego Perini
                            // http://javascript.nwbox.com/IEContentLoaded/
                            top.doScroll("left");
                        } catch(e) {
                            return setTimeout( doScrollCheck, 50 );
                        }
                        // detach all dom ready events
                        detach();
                        done();
                    }
                })();
            }
        }
    })();
    return function(callback){
        Q.queue(function(){callback();Q.dequeue()});
    };
}();

function Loader(){
    var self = this;
    self.__files__  = [];
    self.__package__= [];
    self.__queue__  = true;
    self.__cache__  = true;
    self.__charset__= null;
    self.__delay__  = 0;
}
var LP = Loader.prototype = {
    /**
     * 设置文件编码
     * @method  charset(charset=null)
     * @param   {String} charset    文件编码，缺省null
     * @return  {Loader}            返回当前Loader对象
     * @example
     *     Loader.js("js/app.js").charset("gb2312").run(function(){alert("Hello!")});
     */
    charset : function(charset){this.__charset__ = charset; return this},
    /**
     * 标志是否依次加载
     * @method      queue(queue=true)
     * @param       {Boolean}       queue   true为队列形式依次加载，false则全部同时加载，缺省true
     * @return      {Loader}                返回当前Loader对象
     * @exception                           为了保证js文件的安全加载，我们默认设置的是true，当您的脚本(多个一起加载)并没有顺序要求的时候，建议设置false同时加载速度更快。
     *                                      文件加载的顺序是：模块包优先，其次到本次添加的文件，相应模块或本次文件的加载顺序以添加的先后顺序为加载顺序
     *                                      注意：此方法对模块包的加载是无效，模块包的请在define的时候进行设置
     * @example
     *     Loader.js("js/app.js").queue(true).run(function(){
     *         alert("Hello!");
     *     });
     */
    queue   : function(queue){this.__queue__    = queue; return this},
    /**
     * 延迟一段时间后才执行
     * @method      delay(delay=0)
     * @param       {Number}       delay 延迟时间，单位：毫秒，缺省0
     * @return      {Loader}       返回当前Loader对象
     * @example
     *     Loader.delay(1000).js("js/app.js").run(function(){alert("Hello!")});
     */
    delay   : function(delay){this.__delay__ = delay; return this},
    /**
     * 是否启用缓存
     * @method      cache(cache=true)
     * @param       {Boolean}       cache 当设置为true时候，如果发现文件已被加载过，则不再加载文件，
     *                                    当为false时候表示不缓存文件，即便文件已被加载过，依然再次加载
     *                                    (但是仍可被浏览器缓存，如需绕过浏览器缓存，还需自行在文件地址做手脚，如：加版本号等)
     * @example
     *     Loader.cache(false).js("js/app.js").run(function(){alert("取消缓存后，多次执行此代码时候，app.js中的代码也会被多次执行哦!")});
     */
    cache   : function(cache){this.__cache__ = cache; return this}
};

/**
 * 添加css或js文件
 * @method  add(...args)
 * @description
 *     Loader.add("app1.js","style1.css"...);
 *     可变长参数，支持的参数规则：
 *     1、字符串(会自动以后缀判断是css还js文件，如果后缀不符合css，则认为是js)
 *     2、数组(其实用数组并没有意义，它会自动遍历数组，将数组元素当参数用)
 *     3、Object对象，完整的写法，一个文件一个对象，支持单独设置文件的属性，完整属性如下：
 *         url         必填，字符串类型，指向目标文件路径
 *         type        选填，字符串类型，css或js，如果缺省，则自动以url后缀判断
 *         charset     选填，字符串，设置后将不受Loader.charset方法的控制
 *         cache       选填，布尔值，只对js文件有效，与cache方法同理，不同的是此项只针对当前文件，并且设置了以后，将忽略cache方法中的设置
 *
 * @return    {Loader}   返回当前Loader对象
 * @example
 *      Loader.add({type:"js", url:"filename.js"}).run(function(){alert("加载完成")});
 */
LP.add = function(){
    var file,type,self = this,
        list = [].slice.call(arguments);
    each([].slice.call(arguments), function(file){
        if( Is.Arr(file) ){
            LP.add.apply(self, file);
        }else{
            var meta;
            if( Is.Str(file) ){
                meta = {
                    // 从文件后缀中识别出是js还是css
                    type : (file.replace(/[\?#].*/, '').split(/\./).pop().toLowerCase()=='css') ? 'css' : 'js',
                    url  : file
                }
            }else if(Is.Obj(file) && file.url && Is.Str(file.url)){
                var type = Is.Str(file.type) ? file.type.toLowerCase() : 0;
                if(type!="js" && type!="css"){
                    type = file.url.replace(/[\?#].*/, '').split(/\./).pop().toLowerCase()=='css' ? 'css' : 'js';
                }
                meta = {
                    type    : type,
                    url     : file.url,
                    charset : file.charset,
                    cache   : file.cache
                };
            }
            meta && self.__files__.push(meta);
        }
    });
    return self;
};
/**
 * 添加css文件
 * @method  css(...args)
 * @description
 *     Loader.css("style1.css","style2.css"...);
 *     可变长参数，支持的参数规则：
 *     1、字符串，目标css文件路径
 *     2、Object对象，完整的写法，与add规则区别就是没有type的设置，完整属性如下：
 *          url         必填，字符串类型，指向目标文件路径
 *          charset     选填，字符串，设置后将不受Loader.charset方法的控制
 *          css文件cache设置恒定为true
 * @return    {Loader}   返回当前Loader对象
 * @example
 *     Loader.css({ url:"filename.css", charset:"gb2312" }).run(function(){alert("加载完成")});
 */
LP.css = function(){
    var self = this;
    each([].slice.call(arguments), function(file){
        if( Is.Str(file) ){
            self.add({ type:'css', url:file });
        }else if( Is.Obj(file) ){
            if( Is.Str(file.url) ){
                self.add({
                    type    :'css',
                    url     : file.url,
                    charset : file.charset,
                    cache   : true
                });
            }
        }
    });
    return self;
};
/**
 * 添加js文件
 * @method  js(...args)
 * @description
 *     Loader.js("app1.js","app2.js"...);
 *     可变长参数，支持的参数规则：
 *     1、字符串，目标js文件路径
 *     2、Object对象，完整的写法，与add规则区别就是没有type的设置，完整属性如下：
 *          url         必填，字符串类型，指向目标文件路径
 *          charset     选填，字符串，设置后将不受Loader.charset方法的控制
 *          cache       选填，布尔值，设置后将不受Loader.cache方法的控制
 * @return    {Loader}   返回当前Loader对象
 * @example
 *     Loader.js({ url:"filename.js", charset:"gb2312" }).run(function(){alert("加载完成")});
 */
LP.js = function(){
    var self = this;
    each([].slice.call(arguments), function(file){
        if( Is.Str(file) ){
            self.add({ type:'js', url:file });
        }else if( Is.Obj(file) ){
            if( Is.Str(file.url) ){
                self.add({
                    type    :'js',
                    url     : file.url,
                    charset : file.charset,
                    cache   : file.cache
                });
            }
        }
    });
    return self;
};
/**
 * 添加内嵌样式(即时生效)
 * @method  style(str)
 * @param   {String}    样式表内容
 * @return  {Loader}    返回当前Loader对象
 * @example
 *     Loader.style("a{color:red;}");
 */
LP.style = function(str){
    var css = document.getElementById('CL-Loader-inline-css');
    if (!css) {
        css = document.createElement('style');
        css.type = 'text/css';
        css.id = 'CL-Loader-inline-css';
        document.getElementsByTagName('head')[0].appendChild(css);
    }

    if (css.styleSheet) {
        css.styleSheet.cssText = css.styleSheet.cssText + str;
    } else {
        css.appendChild(document.createTextNode(str));
    }
    return this;
};
/**
 * 选择依赖的模块包（可同时依赖多个包）
 * @method use(...args)
 * @description
 *     Loader.use("pkg1", "pkg2", "pkg3"..)
 *     Loader.use(["pkg1", "pkg2", "pkg3"..])
 *     也可以这么写 Loader.use("pkg1").use("pkg2")....
 *     参数无严格要求，可以单个字符串参数，也可以可变长参数，
 *     甚至还可以用数组(1.1.1新增)，如果遇到数组，则会自动遍历数组
 *     模块包是来源于Loader.define定义的包
 * @return    {Loader}   返回当前Loader对象
 * @example
 *     Loader.use("myPackage").run(function(){alert("我这里需要myPackage包支持哦")});
 */
LP.use = function(){
    var self = this;
    each([].slice.call(arguments), function(n){
        if( Is.Str(n) )
            self.__package__.push(n);
        else if( Is.Arr(n) ){
            LP.use.apply(self, n);
        }
    });
    return self;
};
/**
 * 开始执行加载
 * @method run(...arg)
 * @description
 *     可变长个参数，参数无顺序，
 *         object、array、string类型会自动调用add，可多个参数，会统一收集到加载列表中
 *         number类型则当delay设置，多个参数后者会覆盖前者
 *         boolean类型当queue设置，多个参数后者会覆盖前者
 *         function类型则当回调函数，多个参数后者会覆盖前者
 *         注意：run方法中不支持cache的设置
 * @return  {Undefined}
 * @example
 *     Loader.run(["filename.js", "filename2.js"], false, function(){
 *         alert("Hello!");
 *     });
 **/
LP.run = function(){
    var self = this,
        callback = function(){},
        args = [].slice.call(arguments);
    for(var i=0; i<args.length; i++){
        switch(Is.type(args[i])){
            case 'object':
            case 'string':
                self.add(args[i]);
                break;
            case 'number':
                self.delay(args[i]);
                break;
            case 'boolean':
                self.queue(args[i]);
                break;
            case 'function':
                callback = args[i];
                break;
        }
    }
    Is.type(self.__delay__)=="number" ? setTimeout( function(){_run(self, callback)}, self.__delay__ ) : _run(self, callback);
};
/**
 * 等待到DOM加载完后才开始执行加载
 * @method ready(...arg)
 * @see run
 * @return  {Undefined}
 */
LP.ready = function(){
    var self = this,args=[].slice.call(arguments);
    Ready(function(){
        LP.run.apply(self,args);
    });
};

var _run = function(self, callback){
    if(self.__package__.length>0){/*需要依赖包*/
        var Q = new Queue();
        each(self.__package__, function(name){
            if(!Is.Str(name)){
                console.error("模块名不合法："+Is.type(name)+"不为string");
            }else{
                var pkg = packagesCache.hasOwnProperty(name) ? packagesCache[name] : false;
                if(!pkg){
                    console.error("模块名“"+name+"”未定义！");
                }else{
                    Q.queue(function(){
                        Is.Fun(pkg.onbefore) && pkg.onbefore();
                        new Loader().use(pkg.require).add(pkg.files).charset(pkg.charset).queue(pkg.queue).cache(pkg.cache).run(function(){
                            Q.dequeue();
                            Is.Fun(pkg.onafter) && pkg.onafter();
                        });
                    });
                }
            }
        });
        /*包加载完成后，加载本次添加的文件*/
        Q.queue(function(){
            _load.start(self.__files__, self.__charset__, self.__queue__, self.__cache__,  callback);
            Q.dequeue();
        });
    }else{/*普通加载*/
        _load.start(self.__files__, self.__charset__, self.__queue__, self.__cache__,  callback);
    }
};
each(["add", "css", "js", "use", "charset", "queue", "style", "delay", "ready", "run"], function(name){
    CL_Loader[name] = function(){return LP[name].apply(new Loader(), [].slice.call(arguments))};
});
var packagesCache = {};
/**
 * 包定义或判断某包是否被定义（包定义后不可修改或删除、替换）
 * @method define(p, ...args)
 * @param {Object|String}   p   必填
 *     p为字符串时候判断p是否已被定义，例：  Loader.define("jQuery") 当已经定义jQuery的话返回true，否则false。
 *     p为对象的时候，定义包(定义成功返回true，失败返回false)：
 *     p.name       必填，包名
 *     p.files      必填，同Loader.add()规则，可以是字符串、数组、对象(原始对象)
 *     p.rely       选填，包依赖，不提倡使用了，请看require
 *     p.require    选填，包依赖，可以是字符串或数组。实际就是rely的新别名，同时rely已不再提倡使用，如果require与rely同时存在，则忽略rely而使用require
 *     p.queue      选填，标记是否队列依赖加载，加载顺序为file数组的顺序(缺省true)
 *     p.cache      选填，标记是否使用缓存，详情参考cache方法(缺省true)
 *     p.auto       选填，标记是否在dom完成时候自动加载(缺省false表示不加重)
 *     p.onbefore   选填，包在加载前执行的方法
 *     p.onafter    选填，包加载完成后执行的方法
 *     支持可变长参数进行批量定义，当批量定义时候，本方法将没有返回值
 * @return {Boolean} 定义包时，成功返回true，失败返回false；查询包时，存在返回true，不存在返回false
 *
 * @example
 *     Loader.define({
 *         name : "MyPack",
 *         files  : "filename.js"
 *     });
 *
 * @example
 *     文件没有后缀是无法自动识别类型的，仅使用字符串会被默认为js文件，为此使用对象设置type为css，使加载器知道目标url是个css文件
 *     Loader.define({
 *         name : "MyPack",
 *         files  : { type:"css", url:"http://static.xxxx.com/aefb7675sad" }
 *     });
 *
 * @example
 *     Loader.define({
 *         name : "MyPack",
 *         files  : [
 *             "filename1.js",
 *             "filename2.js",
 *             "filename1.css",
 *             "filename2.css",
 *             { type:"js",  charset:"gb2312", url:"filename3.js"  },
 *             { type:"css", charset:"gb2312", url:"filename3.css" }
 *         ]
 *     });
 **/
CL_Loader.define = function(p){
    // 批量定义
    if(arguments.length>1){
        var args=[].slice.call(arguments);
        for(var i=0; i<args.length; i++){
            CL_Loader.define(args[i]);
        }
    }
    if( Is.Str(p) )
        return packagesCache.hasOwnProperty(p);
    else if( !Is.Obj(p) || !Is.Str(p.name) || this.define(p.name) ){
        return false;
    }
    var d = {status:'',Q:null};
    packagesCache[p.name] = d;
    switch( Is.type(p.files) ){
        case 'string':
        case 'object':
        case 'array':
            d.files = p.files;
            break;
        default:
            d.files = [];
    }
    var require = p.require || p.rely;
    switch( Is.type(require) ){
        case 'string':
        case 'array':
            d.require = p.require;
            break;
        default:
            d.require = null;
    }
    if( p.queue===false )
        d.queue = p.queue;
    if( p.cache===false )
        d.cache = p.cache;
    if( p.charset )
        d.charset = p.charset;
    if( Is.Fun(p.onbefore) )
        d.onbefore = p.onbefore;
    if( Is.Fun(p.onafter) )
        d.onafter = p.onafter;
    if( p.auto )
        this.use(p.name).ready();
    return true;
};

// RequireJS && SeaJS && GlightJS
// GlightJS: https://git.oschina.net/luchg/Glight.js.git
if(typeof define==="function"){
    define(function(){return CL_Loader});
// NodeJS
}else if(typeof exports!=="undefined"){
    module.exports = CL_Loader;
}else{
    window.CL_Loader = window.Loader = window.L = CL_Loader;
}
}(window,document);
