# Loader动态加载库api

> 基本用法

```javascript
/* 
 * 会自动加载filename.js文件，并在加载完成后执行run里面的函数 
 */  
Loader.js("filename.js").run(function(){
    alert("加载完成");
});
```

## 方法合集
* [run](#run)          开始加载文件
* [ready](#ready)      等待DOM完成加载后开始加载文件
* [js](#js)            添加js文件到加载队列
* [css](#css)          添加css文件到加载队列
* [add](#add)          添加js文件或css文件到加载队列
* [charset](#charset)  设置默认文件编码
* [delay](#delay)      延迟执行
* [queue](#queue)      依次加载
* [cache](#cache)      是否使用缓存(v1.2.0新增)
* [style](#style)      即时生效的添加样式表
* [define](#define)    高级功能：模块包定义
* [use](#use)          高级功能：依赖包执行

### run
> 开始执行加载

```javascript
/**
 * @method run(...arg)
 * @description 
 *     可变长个参数，参数无顺序，
 *         object、array、string类型会自动调用add，可多个参数，会统一收集到加载列表中
 *         number类型则当delay设置，多个参数后者会覆盖前者
 *         boolean类型当queue设置，多个参数后者会覆盖前者
 *         function类型则当回调函数，多个参数后者会覆盖前者
 *         注意：run方法中不支持cache的设置
 * @return  {Undefined}
 */   
Loader.run(["filename.js", "filename2.js"], false, function(){
    alert("Hello!");
});
```

### ready
> 等待到DOM加载完后才开始执行加载

```javascript
/**
 * @method ready(...arg)
 * @see run
 * @return  {Undefined}
 */   
Loader.ready(["filename.js", "filename2.js"], false, function(){
    alert("Hello!");
});
```

### js
> 添加js文件

```javascript
/**
 * @method  js(...args)
 * @description 
 *     Loader.js("app1.js","app2.js"...);
 *     可变长参数，支持的参数规则：
 *     1、字符串，目标js文件路径
 *     2、Object对象，完整的写法，与add规则区别就是没有type的设置，完整属性如下：
 *          url         必填，字符串类型，指向目标文件路径
 *          charset     选填，字符串，设置后将不受Loader.charset方法的控制
 *          cache       选填，布尔值，设置后将不受Loader.cache方法的控制
 * @return    {Loader}   返回当前Loader对象
 */
Loader.js({ url:"filename.js", charset:"gb2312" }).run(function(){alert("加载完成")});
// 支持同时添加多个js文件
Loader.js("filename.js","filename2.js").run(function(){alert("加载完成")})    
// 也可以这么多次连贯操作
Loader.js("filename.js").js("filename2.js").run(function(){alert("加载完成")}); 
```

### css
> 添加css文件

```javascript
/**
 * @method  css(...args)
 * @description 
 *     Loader.css("style1.css","style2.css"...);
 *     可变长参数，支持的参数规则：
 *     1、字符串，目标css文件路径
 *     2、Object对象，完整的写法，与add规则区别就是没有type的设置，完整属性如下：
 *          url         必填，字符串类型，指向目标文件路径
 *          charset     选填，字符串，设置后将不受Loader.charset方法的控制
 *          css文件cache不可设置，加载器会将css的cache值恒定为true
 * @return    {Loader}   返回当前Loader对象
 */  
Loader.css({ url:"filename.css", charset:"gb2312" }).run(function(){alert("加载完成")});
// 支持同时添加多个css文件
Loader.css("filename.css","filename2.css").run(function(){alert("加载完成")})    
// 也可以这么多次连贯操作
Loader.css("filename.css").css("filename2.css").run(function(){alert("加载完成")});   
```

# add
> 添加css或js文件

```javascript
/**
 * @method  add(...args)
 * @description 
 *     Loader.add("app1.js","style1.css"...);
 *     可变长参数，支持的参数规则：
 *     1、字符串(会自动以后缀判断是css还js文件，如果后缀不符合css，则认为是js)
 *     2、数组(其实用数组并没有意义，它会自动遍历数组，将数组元素当参数用)
 *     3、Object对象，完整的写法，一个文件一个对象，支持单独设置文件的属性，完整属性如下：
 *         url         必填，字符串类型，指向目标文件路径
 *         type        选填，字符串类型，css或js，如果缺省，则自动以url后缀判断
 *         charset     选填，字符串，设置后将不受Loader.charset方法的控制
 *         cache       选填，布尔值，只对js文件有效，与cache方法同理，不同的是此项只针对当前文件，并且设置了以后，将忽略cache方法中的设置
 *
 * @return    {Loader}   返回当前Loader对象
 */
Loader.add({type:"js", url:"filename.js"}).run(function(){alert("加载完成")});
// 直接使用字符串（自动识别后缀，不为css后缀的一律认为是js文件）
Loader.add("filename.js").run(function(){alert("加载完成")});
```

# charset
> 设置文件编码

```javascript
/**
 * @method  charset(charset=null)
 * @param   {String} charset    文件编码，缺省null
 * @return  {Loader}            返回当前Loader对象
 */
Loader.js("filename.js").charset("gb2312").run(function(){alert("加载完成")});
```

# delay
> 延迟一段时间后才执行

```javascript
/**
 * @method      delay(delay=0)
 * @param       {Number}       delay 延迟时间，单位：毫秒，缺省0
 * @return      {Loader}       返回当前Loader对象
 * @example
 *     
 */  
Loader.delay(1000).js("filename.js").run(function(){alert("加载完成")});  
```

# queue
> 标志是否依次加载

```javascript
/**
 * @method      queue(queue=true)
 * @param       {Boolean}       queue   true为队列形式依次加载，false则全部同时加载，缺省true
 * @return      {Loader}                返回当前Loader对象
 * @exception                           为了保证js文件的安全加载，我们默认设置的是true，当您的脚本(多个一起加载)并没有顺序要求的时候，建议设置false同时加载速度更快。
 *                                      文件加载的顺序是：模块包优先，其次到本次添加的文件，相应模块或本次文件的加载顺序以添加的先后顺序为加载顺序
 *                                      注意：此方法对模块包的加载是无效，模块包的请在define的时候进行设置
 */
// 下面的例子里filename.js和filename2.js并没有加载的先后顺序，所以可能是filename.js先完成加载，也可能是filename2.js先完成加载，具体看网速     
Loader.js("filename.js","filename2.js").queue(false).run(function(){alert("加载完成")});
```

# cache
> 是否启用缓存

```javascript
/**
 * @method      cache(cache=true)
 * @param       {Boolean}       cache 当设置为true时候，如果发现文件已被加载过，则不再加载文件，
 *                                    当为false时候表示不缓存文件，即便文件已被加载过，依然再次加载
 *                                    (但是仍可被浏览器缓存，如需绕过浏览器缓存，还需自行在文件地址做手脚，如：加版本号等)
 */
Loader.cache(false).js("filename.js").run(function(){alert("取消缓存后，多次执行此代码时候，filename.js中的代码也会被多次执行哦!")});
```

# style
> 添加内嵌样式(即时生效)

```javascript
/**
 * @method  style(str)
 * @param   {String}    样式表内容
 * @return  {Loader}    返回当前Loader对象
 */
Loader.style("a{color:red;}"); 
```

# define
> 高级功能：包定义或判断某包是否被定义（包定义后不可修改或删除、替换）

```javascript
/**
 * @method define(p)
 * @param {Object|String}   p   必填
 *     p为字符串时候判断p是否已被定义，例：  Loader.define("jQuery") 当已经定义jQuery的话返回true，否则false。
 *     p为对象的时候，定义包(定义成功返回true，失败返回false)：
 *     p.name       必填，包名
 *     p.files      必填，同Loader.add()规则，可以是字符串、数组、对象(原始对象)
 *     p.rely       选填，包依赖，不提倡使用了，请看require
 *     p.require    选填，包依赖，可以是字符串或数组。实际就是rely的新别名，同时rely已不再提倡使用，如果require与rely同时存在，则忽略rely而使用require
 *     p.queue      选填，标记是否队列依赖加载，加载顺序为file数组的顺序(缺省true)
 *     p.cache      选填，标记是否使用缓存，详情参考cache方法(缺省true)
 *     p.auto       选填，标记是否在dom完成时候自动加载(缺省false表示不加重)
 *     p.onbefore   选填，包在加载前执行的方法
 *     p.onafter    选填，包加载完成后执行的方法
 * @return {Boolean} 定义包时，成功返回true，失败返回false；查询包时，存在返回true，不存在返回false
 */
Loader.define({
    name : "MyPack",
    files  : "filename.js"
});
// 文件没有后缀是无法自动识别类型的，仅使用字符串会被默认为js文件，为此使用对象设置type为css，使加载器知道目标url是个css文件
Loader.define({
    name : "MyPack",
    files  : { type:"css", url:"http://static.xxxx.com/aefb7675sad" }       
});
Loader.define({
    name : "MyPack",
    files  : [
        "filename1.js",
        "filename2.js",
        "filename1.css",
        "filename2.css",
        { type:"js",  charset:"gb2312", url:"filename3.js"  },
        { type:"css", charset:"gb2312", url:"filename3.css" }
    ]
});
```

# package
> 高级功能：选择依赖的模块包（可同时依赖多个包）

```javascript
/**
 * @method use(...args)
 * @description 
 *     Loader.use("pkg1", "pkg2", "pkg3"..)
 *     Loader.use(["pkg1", "pkg2", "pkg3"..])
 *     也可以这么写 Loader.use("pkg1").use("pkg2")....
 *     参数无严格要求，可以单个字符串参数，也可以可变长参数，
 *     甚至还可以用数组(1.1.1新增)，如果遇到数组，则会自动遍历数组
 *     模块包是来源于Loader.define定义的包
 * @return    {Loader}   返回当前Loader对象
 */
Loader.use("myPackage").run(function(){alert("我这里需要myPackage包支持哦")});
```